# Portafolio

Mi experiencia en el desarollo de web.

```js
"devDependencies": {
    "@babel/core": "^7.7.7",
    "@babel/plugin-proposal-class-properties": "^7.7.4",
    "@babel/plugin-syntax-dynamic-import": "^7.7.4",
    "@babel/plugin-transform-runtime": "^7.7.6",
    "@babel/preset-env": "^7.7.7",
    "@babel/preset-react": "^7.7.4",
    "add-asset-html-webpack-plugin": "^3.1.3",
    "autoprefixer": "^9.7.3",
    "babel-loader": "^8.0.6",
    "clean-webpack-plugin": "^3.0.0",
    "css-loader": "^3.4.0",
    "faker": "^4.1.0",
    "file-loader": "^5.0.2",
    "html-webpack-plugin": "^3.2.0",
    "less": "^3.10.3",
    "less-loader": "^5.0.0",
    "md5": "^2.2.1",
    "mini-css-extract-plugin": "^0.9.0",
    "node-sass": "^4.13.0",
    "optimize-css-assets-webpack-plugin": "^5.0.3",
    "postcss": "^7.0.26",
    "postcss-apply": "^0.12.0",
    "postcss-color-function": "^4.1.0",
    "postcss-color-hex-alpha": "^5.0.3",
    "postcss-color-hwb": "^3.0.0",
    "postcss-css-variables": "^0.14.0",
    "postcss-custom-media": "^7.0.8",
    "postcss-custom-selectors": "^5.1.2",
    "postcss-font-magician": "^2.3.1",
    "postcss-image-set-function": "^3.0.1",
    "postcss-import": "^12.0.1",
    "postcss-loader": "^3.0.0",
    "postcss-media-variables": "^2.0.1",
    "postcss-mixins": "^6.2.3",
    "postcss-nested": "^4.2.1",
    "postcss-nesting": "^7.0.1",
    "postcss-partial-import": "^4.1.0",
    "postcss-preset-env": "^6.7.0",
    "postcss-pseudo-class-any-link": "^6.0.0",
    "postcss-reporter": "^6.0.1",
    "postcss-simple-vars": "^5.0.2",
    "sass-loader": "^8.0.0",
    "sort-css-media-queries": "^1.5.0",
    "style-loader": "^1.1.2",
    "stylelint": "^13.0.0",
    "stylus": "^0.54.7",
    "stylus-loader": "^3.0.2",
    "terser-webpack-plugin": "^2.3.1",
    "url-loader": "^3.0.0",
    "webpack": "^4.41.5",
    "webpack-cli": "^3.3.10",
    "webpack-dev-server": "^3.10.1"
  },
  "dependencies": {
    "@babel/runtime": "^7.7.7",
    "@fortawesome/fontawesome-svg-core": "^1.2.26",
    "@fortawesome/free-brands-svg-icons": "^5.12.0",
    "@fortawesome/free-regular-svg-icons": "^5.12.0",
    "@fortawesome/free-solid-svg-icons": "^5.12.0",
    "@fortawesome/react-fontawesome": "^0.1.8",
    "bootstrap": "^4.4.1",
    "font-awesome": "^4.7.0",
    "formik": "^2.1.4",
    "react": "^16.12.0",
    "react-dom": "^16.12.0",
    "react-router-dom": "^5.1.2",
    "styled-components": "^5.0.1",
    "yup": "^0.28.1"
  },
```

# Dependencias de Desarrollo

# Dependecias de Produccion

```bash
 npm i bootstrap react react-dom react-router-dom autoprefixer faker md5 postcss postcss-apply postcss-color-function postcss-color-hex-alpha postcss-color-hwb postcss-css-variables postcss-custom-media postcss-custom-selectors postcss-font-magician postcss-image-set-function postcss-import postcss-loader
 postcss-media-variables postcss-mixins postcss-nested postcss-nesting postcss-partial-import postcss-preset-env postcss-pseudo-class-any-link postcss-reporter postcss-simple-vars sort-css-media-queries @fortawesome/fontawesome-svg-core @fortawesome/free-brands-svg-icons @fortawesome/free-regular-svg-icons @fortawesome/free-solid-svg-icons @fortawesome/react-fontawesome font-awesome



```

    npm i -D style-loader stylelint stylus stylus-loader url-loader file-loader html-webpack-plugin less less-loader babel-loader css-loader sass-loader node-sass @babel/runtime @babel/core @babel/plugin-proposal-class-properties @babel/plugin-syntax-dynamic-import @babel/plugin-transform-runtime @babel/preset-env @babel/preset-react @babel/runtime formik styled-components yup webpack webpack-cli
    webpack-dev-server clean-webpack-plugin terser-webpack-plugin optimize-css-assets-webpack-plugin add-asset-html-webpack-plugin mini-css-extract-plugin
    "

- npm install webpack --save-dev --save-exact
- npm install webpack-cli -D -E
- npm i -D css-loader style-loader
- npm install mini-css-extract-plugin
- npm install html-webpack-plugin
- npm install -D babel-loader @babel/core @babel/plugin-proposal-class-properties
  @babel/plugin-transform-runtime @babel/preset-env
  @babel/preset-react
- npm install -S @babel/runtime
- npm i -D clean-webpack-plugin terser-webpack-plugin optimize-css-assets-webpack-plugin

# Multiple ssh key

```bash
=======
ssh-copy-id -i ~/.ssh/heroblack hackchan@192.168.18.22
ssh-keygen -f ~/.ssh/heroblack -t rsa -b 4096 -C “inge.fabio.rojas@gmail.com”

ssh-keygen -f ~/.ssh/hackchan -t rsa -b 4096 -C “fabiorojas7@gmail.com”

eval `ssh-agent -s`
#adiciona una entrada de ssh-agent
ssh-add heroblack
ssh-add ~/.ssh/hackchan
#lista una entrada de ssh-agent
ssh-add -l
#elimina todas las entradas de ssh-agent
ssh-add -D
#clonando repositorios
git clone git@heroblack:heroblack/repo_name.git
git clone git@hackchan:hackchan/repo_name.git

#local repo
git remote set-url origin git@hackchan:hackchan/repo_name.git
#remoto
git remote add origin git@hackchan:hackchan/repo_name.git
```

# example with hook

```js
Aqui el como se haria con hooks

import React, { useState } from "react";
const BadgeForm = () => {
  const [form, setValues] = useState({
    firstName: "",
    lastName: "",
    email: "",
    jobTitle: "",
    twitter: ""
  });
  const handleInput = event => {

    setValues({
      ...form,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = event => {
    event.preventDefault();
    console.log(form);
  };
  return (
    <>
      <h1>New Attendat</h1>

      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>First Name</label>
          <input
            onChange={handleInput}
            className="form-control"
            type="text"
            name="firstName"
            value={form.firstName}
          />
          <label>Last Name</label>
          <input
            onChange={handleInput}
            className="form-control"
            type="text"
            name="lastName"
            value={form.lastName}
          />
          <label>Email</label>
          <input
            onChange={handleInput}
            className="form-control"
            type="email"
            name="email"
            value={form.email}
          />
          <label>Job Title</label>
          <input
            onChange={handleInput}
            className="form-control"
            type="text"
            name="jobTitle"
            value={form.jobTitle}
          />
          <label>Twitter</label>
          <input
            onChange={handleInput}
            className="form-control"
            type="text"
            name="twitter"
            value={form.twitter}
          />
        </div>

        <button className="btn btn-primary">Save</button>
      </form>
    </>
  );
};

export default BadgeForm;
```
