import * as yup from "yup";
const LoginSchema = yup.object().shape({
  cedula: yup

    .string()
    .required("Cedula es requerida")
    .min(6, "La Cedula debe contener minimo 6 digitos"),
  password: yup.string().required("Password es requerido"),
});

export default LoginSchema;
