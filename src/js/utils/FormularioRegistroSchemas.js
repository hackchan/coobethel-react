import * as yup from "yup";
const SignupSchema = yup.object().shape({
  cedula: yup

    .string()
    .required("Cedula es requerida")
    .min(6, "La Cedula debe contener minimo 6 digitos"),

  tipoDoc: yup
    .string()
    .required("Debe seleccionar un tipo Documento")
    .nullable(),

  primerNombre: yup
    .string()
    .required("El primer nombre es requerido")
    .ensure()
    .trim()
    .matches(
      /^[a-zA-z]{2,16}$/i,
      "El primer nombre solo permite texto y sin espacios",
      { excludeEmptyString: true }
    ),

  segundoNombre: yup
    .string()
    .required("El segundo nombre es requerido")
    .ensure()
    .trim()
    .matches(
      /^[a-zA-z]{2,16}$/i,
      "El primer segundo nombre solo permite texto y sin espacios",
      { excludeEmptyString: true }
    ),

  primerApellido: yup
    .string()
    .required("El primer apellido es requerido")
    .ensure()
    .trim()
    .matches(
      /^[a-zA-z]{2,16}$/i,
      "El primer apellido solo permite texto y sin espacios",
      { excludeEmptyString: true }
    ),

  segundoApellido: yup
    .string()
    .required("El segundo apellido es requerido")
    .ensure()
    .trim()
    .matches(
      /^[a-zA-z]{2,16}$/i,
      "El segundo apellido solo permite texto y sin espacios",
      { excludeEmptyString: true }
    ),

  email: yup
    .string()
    .required("Email es requerido")
    .email("Debe ser un Email valido"),

  celular: yup
    .string()
    .required("celular es requerido")
    .matches(
      /^(300|301|302|303|304|305|315|316|317|318|310|311|312|313|314|320|321|322|323|319|350|351 )[1-9]\d{6}$/i,
      "No es un numero de celular valido"
    ),

  fechaNac: yup
    .date("Debe ser una fecha valida")
    .required("Se debe ingresar la fecha nacimiento")
    .nullable()
    .default(() => {
      return new Date();
    }),

  ppassword: yup.string().required("Password es requerido"),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("ppassword"), null], "Passwords deben coincidir"),
});

export default SignupSchema;
