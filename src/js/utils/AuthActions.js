import { useSelector, useDispatch } from "react-redux";
import Api from "../store/api";

//check token & load users
//const api = new Api();
//Obtener usuario
// async function getUser(cedula) {
//   try {
//     const response = api.list(`/users/?cedula=${cedula}`);
//     const user = response.data.body;
//     dispatch({ type: "USER_LOADED" });
//   } catch (err) {
//     dispatch({ type: "LOGOUT" });
//   }
// }

//dispatch({ type: "USER_LOADING" });
export function getToken() {
  const token = useSelector((state) => state.token);
  return token;
}

export function getListaDocumentos() {
  const tipoDocumentos = useSelector((state) => state.tipoDocumentos);
  return tipoDocumentos;
}

export function setListaDocumentos(listaTipoDocSelect) {
  const dispatch = useDispatch();
  dispatch({
    type: "SET_TIPODOC_LIST",
    payload: listaTipoDocSelect,
  });
}

//headers

// const config = {
//   headers: {
//     "Content-type": "application/json",
//   },
// };

// if (token) {
//   config.headers["x-auth-token"] = token;
// }

// getUser("88249432");
