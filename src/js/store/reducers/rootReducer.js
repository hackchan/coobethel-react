const initialState = {
  tipoDocumentos: [],
  token: null,
  isAuthenticated: null,
  isLoading: false,
  user: null,
  counter: 0,
};

export function rootReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_INCREMENT": {
      return { ...state, counter: counter++ };
    }

    case "SET_TIPODOC_LIST": {
      return { ...state, tipoDocumentos: action.payload };
    }

    case "SET_TOKEN": {
      console.log("SET_TOKEN");
      console.log(action.payload);
      localStorage.setItem("token", action.payload.token);
      return {
        ...state,
        token: action.payload.token,
        isAuthenticated: action.payload.isAuthenticated,
      };
    }

    case "USER_LOADING":
      return {
        ...state,
        isLoading: true,
      };

    case "USER_LOADED":
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false,
        user: action.payload,
      };

    case "LOGIN_SUCESS":
    case "REGISTER_SUCESS":
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        isLoading: false,
      };

    case "LOGOUT":
      localStorage.removeItem("token");
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        isLoading: false,
      };

    default: {
      return state;
    }
  }
}

export default rootReducer;
