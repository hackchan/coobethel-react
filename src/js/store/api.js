import RestApi from "../utils/RestApi";
import config from "../../../config";

class ApiCoobeth {
  constructor() {
    this.url = `http://${config.api.host}:${config.api.port}/api`;
    this.micro = new RestApi(this.url);
  }

  list(table) {
    return this.micro.get(`/${table}`);
  }

  get(endPoint) {
    return this.micro.get(`/${endPoint}`);
  }

  async insert(table, data, header = {}) {
    console.log("La tabla:", table);
    console.log("La data:", data);
    return new Promise(async (resolve, reject) => {
      try {
        let response = await this.micro.post(`/${table}`, data, header);
        resolve(response);
      } catch (err) {
        reject(err.response.data.error.message);
      }
    });
  }

  update(table, campo, id, data) {
    return new Promise(async (resolve, reject) => {
      try {
        let response = this.micro.put(`/${table}/${campo}/${id}`, data);
        resolve(response);
      } catch (err) {
        reject(err.response.data.error.message.message);
      }
    });
  }

  remove(table, campo, id) {
    return this.micro.remove(`/${table}/${campo}/${id}`);
  }

  upsert(table, data) {
    return this.micro.post(`/${table}`, data);
  }
}

export default ApiCoobeth;
