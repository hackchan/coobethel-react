//import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.css";
import "../css/global.css";

//library.add(fab, faCheckSquare, faCoffee);
// import "../css/buttons.css";

// import pokemon from "./api/pokemon/index.js";
//import render from "./render/index.js";

import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import generateStore from "./redux/store";

// import { library } from "@fortawesome/fontawesome-svg-core";
// import { fab } from "@fortawesome/free-brands-svg-icons";
// import { faCheckSquare, faCoffee } from "@fortawesome/free-solid-svg-icons";
// library.add(fab, faCheckSquare, faCoffee);
import App from "./components/App";
// import App2 from "./react-components/components/Routes";
const store = generateStore();
function render() {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById("container")
  );
}

render();

if (module.hot) {
  module.hot.accept("./components/App", function () {
    render();
  });
}

/*
import text from "./text";
text();

if (module.hot) {
  module.hot.accept("./text.js", function() {
    text();
  });
}
/*
const baseUrl = "https://pokeapi.co/api/v2/pokemon/";
const apiPokemon = new pokemon(baseUrl);
apiPokemon
  .search(25)
  .then(data => {
    console.log(data);
    render(data);
  })
  .catch(() => {
    console.log("No hubo pokemon");
  });
*/
