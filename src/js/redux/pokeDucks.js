import api from "../store/api";

const apiRequest = new api();
//constantes
const dataInicial = {
  array: [],
  token: null,
  user: {},
};

//types
const GET_LISTA_TIPODOC = "GET_LISTA_TIPODOC";
const GET_TOKEN = "GET_TOKEN";
const GET_USER = "GET_USER";

//reducer

export default function pokeReducer(state = dataInicial, action) {
  switch (action.type) {
    case GET_LISTA_TIPODOC:
      return { ...state, array: action.payload };
    case GET_TOKEN:
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      return {
        ...state,
        token: action.payload.token,
        user: action.payload.user,
      };
    case GET_USER:
      localStorage.setItem("user", JSON.stringify(action.payload));
      return { ...state, user: action.payload };
    default:
      return state;
  }
}

//acciones

//obtener tipos documentos
export const obtenerListaDocumentosAction = () => async (
  dispatch,
  getState
) => {
  try {
    let request = await apiRequest.list("tipo-doc");
    let tipoDocumentos = request.data.body;
    let listaTipoDocSelect = [];
    tipoDocumentos.map((tipo) => {
      let tipoDoc = {};
      tipoDoc.value = tipo._id;
      tipoDoc.label = `🎫 ${tipo.corto} - ${tipo.nombre}`;
      listaTipoDocSelect.push(tipoDoc);
    });
    dispatch({
      type: GET_LISTA_TIPODOC,
      payload: listaTipoDocSelect,
    });
  } catch (error) {
    console.log(error);
  }
};
//realizar login
export const login = (data) => async (dispatch, getState) => {
  try {
    const token = await apiRequest.insert("users/login", data);
    const usuario = await apiRequest.insert(
      `users/user`,
      { cedula: data.cedula },
      { Authorization: `Bearer ${token.data.body}` }
    );
    dispatch({
      type: GET_TOKEN,
      payload: { token: token.data.body, user: usuario.data.body[0] },
    });
  } catch (error) {
    console.log(error);
  }
};

//obtener usuario logiado
export const getUserLogin = (cedula) => async (dispatch, getState) => {
  try {
    const token = localStorage.getItem("token");
    const response = await apiRequest.insert(
      `users/user`,
      { cedula },
      { Authorization: `Bearer ${token}` }
    );
    console.log("Users:", response);
    dispatch({
      type: GET_USER,
      payload: response.data.body[0],
    });
  } catch (error) {
    console.log(error);
  }
};
