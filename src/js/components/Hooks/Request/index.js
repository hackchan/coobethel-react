import React, { useState, useEffect } from "react";
import Api from "../../../store/api";
//${config.api.host}:${config.api.port}/api/tipo-doc
export const useRequest = (url) => {
  const store = new Api();
  const [data, setData] = useState([]);
  useEffect(() => {
    async function getData() {
      const result = await fetch(url);
      const response = await result.json();
      console.log(response);
      setData(response.body);
    }
    getData();
  }, []);
  return [data];
};
