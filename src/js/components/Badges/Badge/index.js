import React, { Component, Fragment } from "react";
import confLogo from "../../../../image/gender.png";
import "./style.css";

import Gravatar from "../../Gravatar";
class Badge extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Fragment>
        <div className="Badge">
          <div className="Badge__header">
            {/* <div className="ghost">
              <div className="eyes"></div>
              <div className="skirt"></div>
            </div> */}
            {/* <img className="Badge__gender" src={confLogo} alt="logo" /> */}
          </div>

          <div className="Badge__participante">
            <Gravatar
              className="Badge__avatar"
              email={this.props.formValues("email") || "fabiorojas7@gmail.com"}
              alt="Avatar"
            />
            <h1 className="Badge__section-name">
              {this.props.formValues("primerNombre") || "Nombre"}
              {" " + this.props.formValues("segundoNombre")} <br />
              {this.props.formValues("primerApellido") || "Apellido"}
              {" " + this.props.formValues("segundoApellido")} <br />
            </h1>
          </div>

          <div className="Badge__section-info">
            <h5>{this.props.formValues("email") || "Email"}</h5>
            <h6>{this.props.formValues("cedula") || "Cedula"}</h6>
          </div>

          <div className="Badge__footer">
            {this.props.formValues("celular") || "celular"}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Badge;
