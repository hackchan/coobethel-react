import React, { Fragment } from "react";
import "./Footer.css";
export default function Footer() {
  return (
    <Fragment>
      <footer className="Footer">
        <div>
          <p>CoobethelApp 1.0.0</p>
        </div>
        <div>
          <p>
            Diseñado con💚 by
            <a className="link" href="https://twitter.com/hackchan77">
              @hackchan77
            </a>
          </p>
        </div>
      </footer>
    </Fragment>
  );
}
