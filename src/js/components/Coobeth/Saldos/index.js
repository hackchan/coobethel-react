import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const UsersContainer = styled.div`
  margin-left: auto;
  margin-right: auto;
  min-width: 350px;

  @media (min-width: 560px) {
    width: 350px;
  }
`;
const StyledLink = styled(Link)`
  background: transparent;
  text-decoration: none;
  color: #fff;
  padding: 5px;
  grid-column: span 2;
  margin: 0px 30% 4px;
  border: 1px solid #fff;
  border-radius: 5px;
  outline: none;
  &:hover {
    background: #fff;
    color: #00cfff;
  }
`;

const UserTag = styled.div`
  display: grid;
  grid-template-columns: 40% 60%;
  padding: 0;
  grid-column-gap: 5px;
  text-align: center;
  align-items: center;
  border: 1px solid #ccc;
  border-radius: 20px;
  box-shadow: 5px 5px 5px #999;
  margin: 15px 8px;
  border-right: 0px;
  border-bottom: 0px;
`;

const Styledimg = styled.img`
  width: 90%;
  margin: 5%;
`;

const Styled2daColumna = styled.div`
  display: grid;
  grid-template-columns: 0.8fr 1.2fr;
  grid-template-rows: repeat(2, 1fr) 2rem;
  padding: 0;
  text-align: center;
  background: -prefix-linear-gradient(top, #0a91c8, #00cfff);
  background: linear-gradient(to bottom, #0a91c8, #00cfff);
  border-radius: 0px 20px 20px 0px;
  border-left: 5px solid #ccc;
  color: #fff;
`;
const Styled2daColLeft = styled.div`
  color: #fff;
  text-align: left;
  padding: 5px 0px 0px 5px;
  font-size: 0.8rem;
  font-family: "arial, helvetica", sans-serif;
`;

const Saldos = (props) => {
  const user = JSON.parse(localStorage.getItem("user"));

  const url = `http://192.168.18.121:4010/api/coobethel/documento-cliente/${user.cedula}`;
  //const url="http://192.168.18.121:4010/api/coobethel/documento-cliente/1090453826";
  //console.log(url)
  const [data, setData] = useState({
    body: [],
    loading: true,
  });

  useEffect(() => {
    const getData = async () => {
      const res = await fetch(url);
      const datos = await res.json();

      setData({ body: datos.body, loading: false });
    };
    getData();
  }, [url]);

  if (data.loading) {
    return <h1> Loading... </h1>;
  }

  return (
    <UsersContainer>
      <h2> Documentos con Saldo</h2>
      {data.body.map((user) => {
        return (
          <UserTag key={user.DOCUMENTO}>
            <h3>
              <Styledimg
              // src={require("../assets/images/LogoCooBethel.png")}
              ></Styledimg>
              APP
            </h3>

            <Styled2daColumna>
              <Styled2daColLeft>N° Cuenta.</Styled2daColLeft>
              <h4>{user.DOCUMENTO} </h4>
              <Styled2daColLeft>Vlr. Saldo.</Styled2daColLeft>

              <h4>
                {new Intl.NumberFormat("ES-CO", {
                  style: "currency",
                  currency: "COP",
                  minimumFractionDigits: 0,
                }).format(`${user.TOTAL}`)}
              </h4>
              <StyledLink
              // to={`/detalle/${match.params.documento}/${user.DOCUMENTO}/`}
              >
                {" "}
                PAGAR
              </StyledLink>
            </Styled2daColumna>
          </UserTag>
        );
      })}
    </UsersContainer>
  );
};
export default Saldos;
