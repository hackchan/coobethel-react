const { FormContext } = require("react-hook-form");
import React, { Fragment } from "react";
import { Link } from "react-router-dom";

function SesionButton({ name, color }) {
  return (
    <Fragment>
      <Link to="login" className={`button ${color}`}>
        {name}
      </Link>
    </Fragment>
  );
}

export default SesionButton;
