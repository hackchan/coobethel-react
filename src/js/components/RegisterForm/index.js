import React, { useState, Fragment, useRef } from "react";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import Select from "react-select";
import "./RegisterForm.css";
import PageError from "../Errors/PageError";
function RegisterForm(props) {
  const password = useRef({});
  password.current = props.watch("ppassword", "");

  const validateConfirmpassword = async (value) => {
    if (value === password.current) return true;
    return "Password y confirmar password no coinciden";
  };

  const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
  ];

  return (
    <Fragment>
      <div>
        <h2>Registro de Ususario</h2>
        <form onSubmit={props.handleSubmit(props.onSubmit)} action="">
          <div className="form-group">
            {/* CEDULA */}

            <label htmlFor="">Cedula</label>
            <input
              onChange={props.handleChange}
              value={props.register.cedula}
              className="form-control"
              type="text"
              name="cedula"
              ref={props.register({
                required: {
                  value: true,
                  message: "la cedula es requerida",
                },
                minLength: {
                  value: 6,
                  message: "la cedula debe contener minimo 6 digitos",
                },
              })}
            />
            {props.errors.cedula && (
              <p className="error">{props.errors.cedula.message}</p>
            )}
          </div>
          {/* PRIMER NOMBRE */}
          <div className="form-group">
            <label htmlFor="">Primer Nombre</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="text"
              name="primerNombre"
              value={props.register.primerNombre}
              ref={props.register({
                required: {
                  value: true,
                  message: "El primer nombre es requerido",
                },
                pattern: {
                  value: /^[a-zA-z]{2,16}$/i,
                  message: "El primer nombre solo permite texto y sin espacios",
                },
              })}
            />
            {props.errors.primerNombre && (
              <p className="error">{props.errors.primerNombre.message}</p>
            )}
          </div>
          {/* SEGUNDO NOMBRE */}
          <div className="form-group">
            <label htmlFor="">Segundo Nombre</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="text"
              name="segundoNombre"
              value={props.register.segundoNombre}
              ref={props.register({
                required: {
                  value: true,
                  message: "El segundo nombre es requerido",
                },
                pattern: {
                  value: /^[a-zA-z]{2,16}$/i,
                  message: "El segundo nombre solo permite texto sin espacio",
                },
              })}
            />
            {props.errors.segundoNombre && (
              <p className="error">{props.errors.segundoNombre.message}</p>
            )}
          </div>
          {/* PRIMER APELLIDO */}
          <div className="form-group">
            <label htmlFor="">Primer Apellido</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="text"
              name="primerApellido"
              value={props.register.primerApellido}
              ref={props.register({
                required: {
                  value: true,
                  message: "El primer apellido es requerido",
                },
                pattern: {
                  value: /^[a-zA-z]{2,16}$/i,
                  message: "El primer apellido solo permite texto sin espacio",
                },
              })}
            />
            {props.errors.primerApellido && (
              <p className="error">{props.errors.primerApellido.message}</p>
            )}
          </div>
          {/* SEGUNDO APELLIDO */}
          <div className="form-group">
            <label htmlFor="">Segundo Apellido</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="text"
              name="segundoApellido"
              value={props.register.segundoApellido}
              ref={props.register({
                required: {
                  value: false,
                  message: "El segundo apellido es requerido",
                },
                pattern: {
                  value: /^[a-zA-z]{2,16}$/i,
                  message: "El segundo apellido solo permite texto",
                },
              })}
            />
            {props.errors.segundoApellido && (
              <p className="error">{props.errors.segundoApellido.message}</p>
            )}
          </div>
          {/* EMAIL */}
          <div className="form-group">
            <label htmlFor="">Email</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="email"
              name="email"
              value={props.register.email}
              ref={props.register({
                required: { value: true, message: "Email es requerido" },
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  message: "invalid email address",
                },
              })}
            />
            {props.errors.email && (
              <p className="error">{props.errors.email.message}</p>
            )}
          </div>
          {/* CELULAR */}
          <div className="form-group">
            <label htmlFor="">Celular</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="text"
              name="celular"
              value={props.register.celular}
              ref={props.register({
                required: { value: true, message: "El celular es requerido" },
                pattern: {
                  value: /^(300|301|302|303|304|305|315|316|317|318|310|311|312|313|314|320|321|322|323|319|350|351 )[1-9]\d{6}$/i,
                  message: "No es un numero valido",
                },
                maxLength: {
                  value: 10,
                  message: "El celular debe contener 10 digitos",
                },
              })}
            />
            {props.errors.celular && (
              <p className="error">{props.errors.celular.message}</p>
            )}
          </div>
          {/* FECHA NACIMIENTO */}
          <div className="form-group">
            <label htmlFor="">Fecha Nacimiento</label>
            <input
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              type="Date"
              name="fechaNac"
              value={props.register.fechaNac}
              ref={props.register({
                required: {
                  value: true,
                  message: "La fecha de nacimiento es requerida",
                },
              })}
            />
            {props.errors.fechaNac && (
              <p className="error">{props.errors.fechaNac.message}</p>
            )}
          </div>
          {/* GENERO */}
          <div className="form-group">
            <label htmlFor="">Genero</label>
            <select
              name="gender"
              value={props.register.gender}
              // onChange={handleChange}
              onChange={props.handleChange}
              className="form-control"
              ref={props.register({ required: true })}
            >
              <option value="hombre">Hombre</option>
              <option value="mujer">Mujer</option>
            </select>
          </div>
          {/* PASSWORD */}
          <div className="form-group">
            <label htmlFor="">Password</label>
            <input
              // onChange={handleChange}
              //onChange={props.handleChange}
              className="form-control"
              type="password"
              name="ppassword"
              //value={props.register.ppassword}
              ref={props.register({
                required: {
                  value: true,
                  message: "Password es requerido",
                },
                minLength: {
                  value: 8,
                  message: "Password debe tener almenos 8 caracteres",
                },
              })}
            />
            {props.errors.ppassword && (
              <p className="error">{props.errors.ppassword.message}</p>
            )}
          </div>
          {/* CONFIRMAR PASSWORD */}
          <div className="form-group">
            <label htmlFor="">Confirmar Password</label>
            <input
              // onChange={handleChange}
              //onChange={props.handleChange}
              className="form-control"
              type="password"
              name="confirmPassword"
              //value={props.register.confirmPassword}
              ref={props.register({
                required: {
                  value: true,
                  message: "Confirmar password es requerido",
                },
                validate: validateConfirmpassword,
              })}
            />
            {props.errors.confirmPassword && (
              <p className="error">{props.errors.confirmPassword.message}</p>
            )}
          </div>

          <button
            onClick={props.handleSubmit(props.onSubmit)}
            className="btn btn-primary"
          >
            Registrar
          </button>

          {props.requestError && (
            <p className="error">{props.requestError.message}</p>
          )}

          <div className="login-nocount-container">
            <span className="login-nocount">Ya tiene usuario y clave?</span>
            <Link className="login_registrar" to="/login">
              Login
            </Link>
          </div>
        </form>
      </div>
    </Fragment>
  );
}

export default RegisterForm;
