import React, { Fragment, useEffect, useState } from "react";
import { Link, Redirect, useHistory } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import LoginSchema from "../../utils/LoginSchema";
import config from "../../../../config";
import { useSelector, useDispatch } from "react-redux";
import { login, getUserLogin } from "../../redux/pokeDucks";
import PageLoading from "../Loaders/Loader";
import Api from "../../store/api";
import "./LoginForm.css";

function LoginForm(props) {
  const history = useHistory();
  const tipoDocumentos = useSelector((store) => store.tipoDocumentos);
  // console.log("tipoDocumentos:", tipoDocumentos);
  const store = new Api();
  const dispatch = useDispatch();
  const [request, setRequest] = useState({
    loading: false,
    error: null,
  });

  const {
    handleSubmit,
    register,
    errors,
    setValue,
    getValues,
    watch,
    reset,
    control,
  } = useForm({
    validationSchema: LoginSchema,
    defaultValues: {
      cedula: "",
      password: "",
    },
  });

  const onSubmit = async (data) => {
    dispatch(login(data));
    //console.log("props.history:", props.history);

    history.push("/documento/saldo");

    //<Redirect to="/documento/saldo" />;
    //dispatch(getUserLogin(data.cedula));
    /*
    try {
      setRequest({ loading: true, error: null });
      const response = await store.insert("users/login", values);
      const token = response.data.body;
      dispatch({
        type: "SET_TOKEN",
        payload: { token, isAuthenticated: true },
      });

      //props.history.push("/newUser/exito");
      //reset();
      setRequest({ loading: false });
    } catch (error) {
      setRequest({ loading: false, error: error });
    }*/
  };

  if (localStorage.getItem("token")) {
    return <Redirect to="/documento/saldo" />;
  }

  if (request.loading) {
    return <PageLoading />;
  }

  return (
    <Fragment>
      <div className="login-container">
        <div className="login">
          <h2 className="login-title">Iniciar Sesion</h2>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              {/* CEDULA */}

              <label htmlFor="">Cedula</label>
              <input
                className="form-control"
                type="text"
                name="cedula"
                ref={register}
              />
              {errors.cedula && (
                <p className="error">{errors.cedula.message}</p>
              )}
            </div>

            {/* PASSWORD */}

            <div className="form-group">
              <label htmlFor="">Password</label>
              <input
                className="form-control"
                type="password"
                name="password"
                ref={register}
              />
              {errors.password && (
                <p className="error">{errors.password.message}</p>
              )}
            </div>

            <button
              // onClick={props.handleSubmit(props.onSubmit)}
              className="btn-login btn btn-primary input-block-level"
            >
              Login
            </button>
            {request.error && <p className="error">{request.error.message}</p>}
          </form>
          <div className="login-nocount-container">
            <span className="login-nocount">No tiene cuenta todavia?</span>
            <Link className="login_registrar" to="/newUser">
              Registrese ahora.
            </Link>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default LoginForm;
