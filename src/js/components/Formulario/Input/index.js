import React from "react";
import styled from "styled-components";

const InputStyled = styled.label`
  display: flex;
  background: white;
  align-items: center;
  box-shadow: 0 2px 9px 0 rgba(0, 0, 0, 0.5);
  padding: 0 1rem;
  border-radius: 5px;

  i {
    margin-right: 1em;
  }

  input {
    flex: 1;
    border: none;
    outline: 0;
    height: 38px;
    line-height: 38px;
    color: #444;
    font-size: 1.1em;
    &::-webkit-input-placeholder {
      color: #c4c4c4;
    }
  }
`;

function Input({ ...props }) {
  return (
    <InputStyled>
      <i className="fas fa-search"></i>
      <input type="text" {...props} />
    </InputStyled>
  );
}

export default Input;
