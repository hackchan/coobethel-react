import React, { Component, Fragment, useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import Select from "react-select";
import Input from "../Input/";
import makeAnimated from "react-select/animated";
import SignupSchema from "../../../utils/FormularioRegistroSchemas";
import config from "../../../../../config";
import moment from "moment";
import Badge from "../../Badges/Badge";
import Api from "../../../store/api";
import PageLoading from "../../Loaders/Loader";
import { useSelector, useDispatch } from "react-redux";
import { obtenerListaDocumentosAction } from "../../../redux/pokeDucks";
//import { useRequest } from "../../Hooks/Request/";

export function FormularioRegistro(props) {
  const dispatch = useDispatch();
  const tipoDocumentos = useSelector((store) => store.tipoDocumentos.array);
  //const token = useSelector((state) => state.token);

  function customTheme(theme) {
    return {
      ...theme,
      colors: {
        ...theme.colors,
        primary25: "red",
        primary: "#b7d6f9",
      },
    };
  }
  const store = new Api();
  const generos = [
    { value: "hombre", label: "👱‍♂️ Hombre" },
    { value: "mujer", label: "👱‍♀ Mujer" },
  ];

  const [request, setRequest] = useState({
    loading: false,
    error: null,
  });

  const [loading, setLoading] = useState(true);
  const [genero, setGenero] = useState({});
  const [tipoDocument, setTipoDocument] = useState([]);
  const [selectTipoDoc, setSelectTipoDoc] = useState({});

  async function getRemoteListaDocumentos() {
    dispatch(obtenerListaDocumentosAction());
    // const result = await fetch(
    //   `http://${config.api.host}:${config.api.port}/api/tipo-doc`
    // );
    // let response = await result.json();
    // let listaTipoDocSelect = [];
    // response.body.map((tipo) => {
    //   let tipoDoc = {};
    //   tipoDoc.value = tipo._id;
    //   tipoDoc.label = `🎫 ${tipo.corto} - ${tipo.nombre}`;
    //   listaTipoDocSelect.push(tipoDoc);
    // });

    // dispatch({
    //   type: "GET_LISTA_TIPODOC",
    //   payload: listaTipoDocSelect,
    // });

    setLoading(false);
  }

  useEffect(() => {
    getRemoteListaDocumentos();
  }, []);

  const {
    handleSubmit,
    register,
    errors,
    setValue,
    getValues,
    watch,
    reset,
    control,
  } = useForm({
    validationSchema: SignupSchema,
    defaultValues: {
      cedula: "",
      Select: "",
    },
  });

  const values = watch();

  const onSubmit = async (values) => {
    let sendObject = {
      ...values,
      tipoDoc: selectTipoDoc.value,
      genero: genero.value,
      fechaNac: moment(values.fechaNac).format("YYYY-MM-DD"),
    };

    try {
      setRequest({ loading: true, error: null });
      let responseUserSave = await store.insert("users", sendObject);
      props.history.push("/newUser/exito");
      reset();
      setRequest({ loading: false });
    } catch (error) {
      setRequest({ loading: false, error: error });
    }
  };

  if (request.loading) {
    return <PageLoading />;
  }

  return (
    <Fragment>
      {/* <form onSubmit={props.handleSubmit(props.onSubmit)} action=""> */}
      <div className="container">
        <div className="row">
          <div className="col-sm-none col-md">
            <Badge formValues={getValues} />
          </div>
          <div className="col-md">
            <h2>Registro Usuarios</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group">
                <Controller
                  as={
                    <Select
                      options={tipoDocumentos}
                      isClearable={true}
                      isSearchable={true}
                      isLoading={loading}
                    />
                  }
                  control={control}
                  onChange={([selected]) => {
                    // Place your logic here
                    setSelectTipoDoc(selected);
                    return selected;
                  }}
                  name="tipoDoc"
                  // defaultValue={{ value: "1" }}
                />

                {errors.tipoDoc && (
                  <p className="error">{errors.tipoDoc.message}</p>
                )}
              </div>

              <div className="form-group">
                {/* CEDULA */}
                <label htmlFor="">
                  {selectTipoDoc.label
                    ? selectTipoDoc.label.split("-")[1]
                    : "Cedula"}
                </label>
                {/* <Input
                  type="text"
                  name="cedula"
                  placeholder="digite una cedula"
                /> */}
                <input
                  //onChange={onChange}
                  className="form-control"
                  type="text"
                  name="cedula"
                  ref={register}
                />
                {/* <input
                  //onChange={onChange}
                  className="form-control"
                  type="text"
                  name="cedula"
                  ref={register}
                /> */}
                {errors.cedula && (
                  <p className="error">{errors.cedula.message}</p>
                )}
              </div>

              {/* PRIMER NOMBRE */}
              <div className="form-group">
                <label htmlFor="">Primer Nombre</label>
                <input
                  className="form-control"
                  type="text"
                  name="primerNombre"
                  ref={register}
                />
                {errors.primerNombre && (
                  <p className="error">{errors.primerNombre.message}</p>
                )}
              </div>

              {/* SEGUNDO NOMBRE */}
              <div className="form-group">
                <label htmlFor="">Segundo Nombre</label>
                <input
                  className="form-control"
                  type="text"
                  name="segundoNombre"
                  ref={register}
                />
                {errors.segundoNombre && (
                  <p className="error">{errors.segundoNombre.message}</p>
                )}
              </div>

              {/* PRIMER APELLIDO */}
              <div className="form-group">
                <label htmlFor="">Primer Apellido</label>
                <input
                  className="form-control"
                  type="text"
                  name="primerApellido"
                  ref={register}
                />
                {errors.primerApellido && (
                  <p className="error">{errors.primerApellido.message}</p>
                )}
              </div>

              {/* SEGUNDO APELLIDO */}
              <div className="form-group">
                <label htmlFor="">Segundo Apellido</label>
                <input
                  className="form-control"
                  type="text"
                  name="segundoApellido"
                  ref={register}
                />
                {errors.segundoApellido && (
                  <p className="error">{errors.segundoApellido.message}</p>
                )}
              </div>

              {/* EMAIL */}
              <div className="form-group">
                <label htmlFor="">Email</label>
                <input
                  className="form-control"
                  type="email"
                  name="email"
                  ref={register}
                />
                {errors.email && (
                  <p className="error">{errors.email.message}</p>
                )}
              </div>
              {/* CELULAR */}
              <div className="form-group">
                <label htmlFor="">Celular</label>
                <input
                  className="form-control"
                  type="text"
                  name="celular"
                  ref={register}
                />
                {errors.celular && (
                  <p className="error">{errors.celular.message}</p>
                )}
              </div>

              {/* FECHA NACIMIENTO */}
              <div className="form-group">
                <label htmlFor="">Fecha Nacimiento</label>
                <input
                  className="form-control"
                  type="Date"
                  name="fechaNac"
                  ref={register}
                />
                {errors.fechaNac && (
                  <p className="error">{errors.fechaNac.message}</p>
                )}
              </div>
              {/* GENERO */}
              <div className="form-group">
                <label htmlFor="">Genero</label>
                <Controller
                  as={
                    <Select
                      defaultValue={{ value: "hombre", label: "👱‍♂️ Hombre" }}
                      components={makeAnimated()}
                      options={generos}
                      isClearable={false}
                      isSearchable={false}
                    />
                  }
                  control={control}
                  onChange={([selected]) => {
                    // Place your logic here
                    setGenero(selected);
                    return selected;
                  }}
                  name="genero"
                  // defaultValue={{ value: "1" }}
                />

                {errors.genero && (
                  <p className="error">{errors.genero.message}</p>
                )}
              </div>
              {/* PASSWORD */}
              <div className="form-group">
                <label htmlFor="">Password</label>
                <input
                  // onChange={handleChange}
                  //onChange={props.handleChange}
                  className="form-control"
                  type="password"
                  name="ppassword"
                  //value={props.register.ppassword}
                  ref={register}
                />
                {errors.ppassword && (
                  <p className="error">{errors.ppassword.message}</p>
                )}
              </div>
              {/* CONFIRMAR PASSWORD */}
              <div className="form-group">
                <label htmlFor="">Confirmar Password</label>
                <input
                  // onChange={handleChange}
                  //onChange={props.handleChange}
                  className="form-control"
                  type="password"
                  name="confirmPassword"
                  //value={props.register.confirmPassword}
                  ref={register}
                />
                {errors.confirmPassword && (
                  <p className="error">{errors.confirmPassword.message}</p>
                )}
              </div>

              <button
                // onClick={props.handleSubmit(props.onSubmit)}
                className="btn btn-primary"
              >
                Registrar
              </button>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default FormularioRegistro;
