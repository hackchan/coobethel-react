import React from "react";
import "./notFound.css";
import astronauta from "../../../../image/astronauta.png";
import { Link } from "react-router-dom";
function NotFound() {
  return (
    <div className="notFound__error">
      <div className="notFound-details">
        <h1 className="notFound-title">404</h1>
        <p className="notFound">PAGINA NO ENCONTRADA</p>
        <p className="notFoundLost">
          En este mar de estrellas no està lo que buscas
        </p>
        <Link className="button green" to="/login">
          Home
        </Link>
      </div>

      <figure className="notFound-image-container">
        <img src={astronauta} alt="error 404" width="500" />
      </figure>
    </div>
  );
}

export default NotFound;
