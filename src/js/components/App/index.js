import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LayoutBasic from "../Layout/LayoutBasic";
import NewUser from "../pages/NewUser";
import Documents from "../pages/Documents";
import NotFound from "../Errors/NotFound";
import Login from "../pages/Login";
import Badge from "../Badges/Badge";
import BadgeNew from "../Badges/page/BadgeNew";
import BadgeAll from "../Badges/page/BadgeAll";
import BadgeDetailsContainer from "../Badges/page/BadgeDetailsContainer";
import BadgeEdit from "../Badges/page/BadgeEdit";
import Message from "../Message";
import Formulario from "../Formulario";
import PrivateRoute from "../PrivateRoute";
import Saldos from "../../components/Coobeth/Saldos";
const token = localStorage.getItem("token") ? true : false;

function App() {
  return (
    <BrowserRouter>
      <LayoutBasic token={token}>
        <Switch>
          <Route
            exact
            path="/playlist"
            render={(props) => <Login {...props} token={token} />}
          />
          {/* <Route exact path="/" component={Login} /> */}
          <Route exact path="/formulario" component={Formulario} />
          <Route exact path="/newUser" component={NewUser} />
          <Route exact path="/documents" component={Documents} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/badges" component={Badge} />
          <Route exact path="/badges/new" component={BadgeNew} />
          <Route exact path="/badges/all" component={BadgeAll} />
          <Route
            exact
            path="/badges/:badgeId"
            component={BadgeDetailsContainer}
          />
          <Route exact path="/badges/:badgeId/edit" component={BadgeEdit} />
          {/* <PrivateRoute
            exact
            path="/documento/saldo"
            render={(props) => <Login {...props} token={token} />}
          /> */}
          <PrivateRoute exact path="/documento/saldo" component={Saldos} />
          <PrivateRoute exact path="/newUser/exito" component={Message} />
          <Route path="*" component={NotFound} />
        </Switch>
      </LayoutBasic>
    </BrowserRouter>
  );
}

export default App;
