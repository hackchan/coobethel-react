import React, { Component, Fragment } from "react";
import "./bill.css";
class Bill extends Component {
  render() {
    return (
      <Fragment>
        <h6 className="bill-title">Detalle cuotas</h6>
        <div className="bill-container">
          <p className="bill-details">cuota 1 de 2</p>
          <p className="bill-details">02-10-2020</p>
          <p className="bill-details bill-cash">$20.000</p>
        </div>
        <div className="bill-container">
          <p className="bill-details">cuota 1 de 2</p>
          <p className="bill-details">02-10-2020</p>
          <p className="bill-details bill-cash">$20.000</p>
        </div>
        <div className="bill-container">
          <p className="bill-details">cuota 1 de 2</p>
          <p className="bill-details">02-10-2020</p>
          <p className="bill-details bill-cash">$20.000</p>
        </div>
      </Fragment>
    );
  }
}
export default Bill;
