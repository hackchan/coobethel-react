import React, { Component, Fragment } from "react";
import Bill from "../Bill/index";
import "./card.css";
class Card extends Component {
  render() {
    return (
      <Fragment>
        <div className="card-container">
          <div className="card-title">
            <p>{this.props.data.documento}</p>
            <p>{this.props.data.fecha}</p>
          </div>
          <div className="card-description">
            <p>
              <strong>Total Pagar</strong>
            </p>
            <h2>{this.props.data.totalPagar}</h2>
          </div>
          <div className="card-cash">
            <a className="btn btn-green">Pagar</a>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Card;
