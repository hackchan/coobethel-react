import React, { Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import Hero from "../Hero";
function Documents() {
  const tipoDocumentos = useSelector((store) => store.tipoDocumentos.array);
  console.log("tipoDocumentos:", tipoDocumentos);
  return (
    <Fragment>
      <Hero />
    </Fragment>
  );
}

export default Documents;
