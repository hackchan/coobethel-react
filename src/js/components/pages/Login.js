import React, { Fragment } from "react";
import { useForm } from "react-hook-form";
import Hero from "../Hero";
import LoginForm from "../LoginForm";
function Login() {
  const { register, setState, handleSubmit, errors, watch } = useForm({
    defaultValues: {
      cedula: "",
      password: "",
    },
  });
  const onSubmit = (e) => {
    console.log("la data desde page:", register.cedula);

    // console.log("execute onSubmit");
    // console.log("data:", data);
    //e.preventDefault();
  };
  return (
    <Fragment>
      {/* <Hero /> */}
      <LoginForm
        onSubmit={onSubmit}
        handleSubmit={handleSubmit}
        register={register}
        errors={errors}
        watch={watch}
      />
    </Fragment>
  );
}

export default Login;
