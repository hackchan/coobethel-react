import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Hero from "../Hero";
import logo from "../../../image/astronauta-gif-1.png";
function RegisterExito() {
  return (
    <Fragment>
      <Hero />
      <div className="container">
        <div className="row">
          <div className="col-md">
            <img src={logo} />
          </div>
          <div className="col-md">
            <div className="RegisterExito-welcome">
              <div className="details">
                <h4>Felicitaciones su registro se completo con exito</h4>
                <Link to="/login" className="btn btn-primary">
                  Login
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default RegisterExito;
