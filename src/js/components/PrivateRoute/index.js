import React, { Fragment } from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
const PrivateRoute = ({ component: Component, ...rest }) => {
  const dispatch = useDispatch();
  // const tipoDocumentos = useSelector((state) => state.tipoDocumentos);
  const token = localStorage.getItem('token');
  const isAuthenticated = token ? true : false;
  //const isToken = token ? true : false;
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated === true ? (
          <Component {...props} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};

export default PrivateRoute;
