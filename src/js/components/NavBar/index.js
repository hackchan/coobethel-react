import React, { Component, Fragment, useState } from "react";
import logo from "../../../image/twitter.svg";
import { Link } from "react-router-dom";
import SesionButton from "../Buttons/SesionButton";
import styled from "styled-components";
import "./navBar.css";
function NavBar(props) {
  const [burger, setBurger] = useState(false);
  const handleOnClick = (event) => {
    setBurger(!burger);

    // const styles = {
    //   box: {
    //     width: 100,
    //     height: 100,
    //     border: "10px solid chocolate",
    //     borderRadius: 10,
    //   },
    // };
  };

  return (
    <Fragment>
      <header className="header">
        <i
          onClick={handleOnClick}
          className="icon-menu burger-button"
          id="burger-menu"
        ></i>
        <figure href="#" className="logo-container">
          <img src={logo} alt="logo" />
        </figure>
        <div className={`header_wrap ${burger ? "is-active" : ""}`}>
          <nav className="Menu">
            <ul className="Menu-content">
              <li className="Menu-details">
                <a href="#" className="link">
                  Notificaciones
                </a>
              </li>
              <li className="Menu-details">
                <a href="#" className="link">
                  Notificaciones
                </a>
              </li>
              <li className="Menu-details">
                <a href="#" className="link">
                  Notificaciones
                </a>
              </li>
            </ul>
          </nav>
          <SesionButton
            name={props.token ? "Cerrar Sesion" : "Iniciar Sesion"}
            color={props.token ? "red" : "green"}
            token={props.token}
          />
          {/* <Link to="login" className="button green">
            Iniciar Sesion
          </Link> */}
          {/* <i className="icon-user-solid-circle user-icon"></i> */}
        </div>
      </header>
    </Fragment>
  );
}
export default NavBar;
