import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import imageRegister from "../../../image/astronauta-gif-1.png";
import styled from "styled-components";
import estrella from "../../../image/stars.svg";
import Hero from "../Hero";
const MessageStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: center;
  height: 94vh;
  color: white;
  background: #222;
  background-image: url(${estrella});
  @media screen and (max-width: 1023px) {
    position: relative;
    overflow: hidden;
    padding: 2em;
  }

  .Container-app {
    max-width: 1000px;
    flex: 1;
    /* width: 100%; */
    margin: 0 auto;
    display: inherit;
    justify-content: inherit;
    align-items: inherit;
    height: inherit;
    flex-wrap: inherit;
    padding: 0 0.5em;
  }

  img {
    max-width: 450px;
    object-fit: cover;
    @media screen and (max-width: 767px) {
      max-width: 300px;

      z-index: 1;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      width: 100%;
    }
    @media screen and (max-width: 480px) {
      display: none;
    }
  }
  h1 {
    @media screen and (max-width: 1023px) {
      font-size: 2em;
      z-index: 2;
    }
  }
  h1 strong {
    color: var(--hdr-btn-login);
  }
`;
export class Message extends Component {
  render() {
    return (
      <Fragment>
        <MessageStyled>
          <div className="Container-app">
            <div className="wrappr">
              <h1>
                <strong>Bienvendo</strong> tu registro <br />
                se completo con <strong>exito</strong>
              </h1>
              <Link to="/login" className="btn btn-primary">
                Iniciar Sesion
              </Link>
            </div>
            <img src={imageRegister} alt="Registro exitoso" />
          </div>
        </MessageStyled>
      </Fragment>
    );
  }
}

export default Message;
