import React, { Fragment } from "react";
import NavBar from "../../NavBar";
import Footer from "../../Footer";
function LayoutBasic(props) {
  return (
    <Fragment>
      <NavBar token={props.token} />
      {props.children}
      <Footer />
    </Fragment>
  );
}
export default LayoutBasic;
